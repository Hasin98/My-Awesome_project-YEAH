from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
def home(request):
    response={}
    if ('username' in request.session.keys()):
        response['role']=request.session['role']
        if ('nama' in request.session.keys()):
            response['nama']=request.session['nama']
        response['username']=request.session['username']
        return render(request, 'home.html', response)
    else:
        return HttpResponseRedirect(reverse('account:login'))

def indexLogin(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'login.html')

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    result=cursor.fetchone();
    if (result):
        cursor.execute("SELECT password from pengguna where username='"+username+"'")
        result=cursor.fetchone();
        if (result[0]==password):
            request.session['username']=username
            cursor.execute("SELECT role from pengguna where username='"+username+"'")
            result=cursor.fetchone();
            request.session['role'] = result[0]
            if (result[0]!="admin"):
                role=result[0]
                if (role!="mahasiswa"):
                    role="donatur"
                cursor.execute("SELECT nama from " +role+ " where username='"+username+"'")
                result=cursor.fetchone();
                request.session['nama']=result[0]

            return HttpResponseRedirect(reverse('home'))
    messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('account:login'))


def register(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'register.html')

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('account:login'))

def submit_mahasiswa(request):
    username = request.POST['username']
    password = request.POST['password']
    npm  = request.POST['npm']
    email = request.POST['email']
    nama = request.POST['nama_lengkap']
    nomor_telepon = request.POST['nomor_telepon']
    alamat = request.POST['alamat']
    alamat_domi = request.POST['alamat_domi']
    bank = request.POST['bank']
    alamat = request.POST['alamat']
    rekening = request.POST['rekening']
    nama_pemilik = request.POST['nama_pemilik']

    response={}

    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="Username"
            response['isi']=username
            return render(request, 'fail.html', response)

    cursor=connection.cursor()
    cursor.execute("SELECT * from mahasiswa where npm='"+npm+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="NPM"
            response['isi']=npm
            return render(request, 'fail.html', response)

    values = "'"+username+"','"+password+"','mahasiswa'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+npm+"','"+email+"','"+nama+"','"+nomor_telepon+"','"+alamat+"','"+alamat_domi+"','"+bank+"','"+rekening+"','"+nama_pemilik+"','"+username+"'"
    query = "INSERT INTO mahasiswa (npm,email,nama,no_telp,alamat_tinggal,alamat_domisili,nama_bank,no_rekening,nama_pemilik,username) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='mahasiswa'

    return render(request, 'success.html', response)

def submit_individual_donor(request):
    username = request.POST['usernameindividual']
    password = request.POST['passwordindividual']
    ni  = request.POST['niindividual']
    nik = request.POST['nik']
    email = request.POST['emailindividual']
    nama = request.POST['nama_lengkapindividual']
    npwp = request.POST['npwp']
    nomor_telepon = request.POST['nomor_teleponindividual']
    alamat = request.POST['alamatindivual']

    response={}

    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="Username"
            response['isi']=username
            return render(request, 'fail.html', response)

    cursor=connection.cursor()
    cursor.execute("SELECT * from donatur where nomor_identitas = '"+ni+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="Nomor Identitas"
            response['isi']=ni
            return render(request, 'fail.html', response)

    cursor=connection.cursor()
    cursor.execute("SELECT * from individual_donor where nik='"+nik+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="NIK"
            response['isi']=nik
            return render(request, 'fail.html', response)

    values = "'"+username+"','"+password+"','individual_donor'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+ni+"','"+email+"','"+nama+"','"+npwp+"','"+nomor_telepon+"','"+alamat+"','"+username+"'"
    query = "INSERT INTO donatur (nomor_identitas,email,nama,npwp,no_telp,alamat,username) values (" + values + ")"
    cursor.execute(query)

    values = "'"+nik+"','"+ni+"'"
    query = "INSERT INTO individual_donor (nik,nomor_identitas_donatur) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='individual_donor'

    return render(request, 'success.html', response)

def submit_yayasan(request):
    username = request.POST['usernameyayasan']
    password = request.POST['passwordyayasan']
    ni  = request.POST['niyayasan']
    nsk = request.POST['nsk']
    email = request.POST['emailyayasan']
    nama = request.POST['namayayasan']
    nomor_telepon = request.POST['nomor_teleponyayasan']
    npwp = request.POST['npwpyayasan']
    alamat = request.POST['alamatyayasan']

    response={}

    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="Username"
            response['isi']=username
            return render(request, 'fail.html', response)

    cursor=connection.cursor()
    cursor.execute("SELECT * from donatur where nomor_identitas = '"+ni+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="Nomor Identitas"
            response['isi']=ni
            return render(request, 'fail.html', response)

    cursor=connection.cursor()
    cursor.execute("SELECT * from yayasan where no_sk_yayasan='"+nsk+"'")
    result=cursor.fetchone();
    if (result):
        if len(result)>0:
            response['error']="NIK"
            response['isi']=nik
            return render(request, 'fail.html', response)

    values = "'"+username+"','"+password+"','yayasan'"
    query = "INSERT INTO pengguna (username,password,role) values (" + values + ")"
    cursor.execute(query)

    values = "'"+ni+"','"+email+"','"+nama+"','"+npwp+"','"+nomor_telepon+"','"+alamat+"','"+username+"'"
    query = "INSERT INTO donatur (nomor_identitas,email,nama,npwp,no_telp,alamat,username) values (" + values + ")"
    cursor.execute(query)

    values = "'"+nsk+"','"+email+"','"+nama+"','"+ni+"'"
    query = "INSERT INTO yayasan (no_sk_yayasan,email,nama,nomor_identitas_donatur) values (" + values + ")"
    cursor.execute(query)

    request.session['username']=username
    request.session['nama']=nama
    request.session['role']='yayasan'

    return render(request, 'success.html', response)
