from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages

# Create your views here.
def home(request):
    response={}
    if ('username' in request.session.keys()):
        response['role']=request.session['role']
        if ('nama' in request.session.keys()):
            response['nama']=request.session['nama']
        response['username']=request.session['username']
        return render(request, 'home.html', response)
    else:
        return HttpResponseRedirect(reverse('account:login'))

def indexLogin(request):
    if ('username' in request.session.keys()):
        return HttpResponseRedirect(reverse('home'))
    return render(request, 'login.html')

def login(request):
    username = request.POST['username']
    password = request.POST['password']
    cursor=connection.cursor()
    cursor.execute("SELECT * from pengguna where username='"+username+"'")
    result=cursor.fetchone();
    if (result):
        cursor.execute("SELECT password from pengguna where username='"+username+"'")
        result=cursor.fetchone();
        if (result[0]==password):
            request.session['username']=username
            cursor.execute("SELECT role from pengguna where username='"+username+"'")
            result=cursor.fetchone();
            request.session['role'] = result[0]
            if (result[0]!="admin"):
                role=result[0]
                if (role!="mahasiswa"):
                    role="donatur"
                cursor.execute("SELECT nama from " +role+ " where username='"+username+"'")
                result=cursor.fetchone();
                request.session['nama']=result[0]

            return HttpResponseRedirect(reverse('home'))
    messages.error(request, "Username atau password salah")
    return HttpResponseRedirect(reverse('account:login'))